<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'marketone' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'W-f(mjt&/).56Yn:5c<QC^@;u(b.>Y.7k}i+~^o!G^v `CZ[KPkLrd)I%ql*V{_@' );
define( 'SECURE_AUTH_KEY',  'f7>uhrC)4fao:D72vrsTx$k`9xkoG79po?*PO;~]?,0OwRp4!Ex:c#Ct Cdy:Ed$' );
define( 'LOGGED_IN_KEY',    'c3JYE[DP5$eFJ3K:ImMF]4*!C{rJu{*.-ilSW(`Pk/ASyJfek>f|62uj,W?,&AwP' );
define( 'NONCE_KEY',        'Z1pV6_1&-F!DD5irxNfa+yI:vM#4)pzCz*PqcCaz&NnMpbu;W[buT]jykaRs0es8' );
define( 'AUTH_SALT',        '|?T#p],E(UxR$zItw{T*t%hxVk[-.BcT;AkwGUgvpuc%h(^twCBWTD:wi:M^K#.*' );
define( 'SECURE_AUTH_SALT', '28SJHB+-w@KZ~kr4)>,f<zaLMa?Y|BZ]@*rSdP{iP*W~-@*$DP`^,Y 5aCQWX?16' );
define( 'LOGGED_IN_SALT',   'e8`h]_BP/Y.4;de<k.mOpIjVK&gx`1E) LN,:{_?@uwJZ(~e)?QFt%[()p tAJ+I' );
define( 'NONCE_SALT',       'Th1*V&w=y~$CEm=B~c!(_9o#ML>pt>;|@yM8DPI:P)cd<wbCZlw[m8>wqASpEw}1' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
